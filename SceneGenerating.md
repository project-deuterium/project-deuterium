# How to use the Scene Generator
Follow this tutorial to easily use the Scene Generator tool. The tool is very specific and will probably not work if you do not follow these instructions.

### 1. Create the Scene

With the unity project open, create a new Scene scriptableobject under States, name it accordingly. I suggest you name it Scene1, where 1 is the number of said "scene" in the book.
You create a Scene scriptableobject by right clicking in the States folder: *Create -> ScriptableObject -> Scene*.

### 2. Add the Scene to the Generator

Now, go to the Generate Scene. Drag your Scene1 into the scene slot of the Generator.


![There was supposed to be a picture here](https://snipboard.io/9edZVS.jpg "Drag to controller")

### 3. Create wrapper object

Create an empty object called *pappa*. It will work as a "wrapper". All of the prefabs you place must be children of this object, as the generator will only load objects that lie under the wrapper object pappa.

### 4. Create your scene

Now, you can freely create your scene. When you are done you simply press the *Generate* button. Here are a few things you need to keep in mind:

* All of the prefabs must lie in the *Resource* folder. Otherwise the templateLoader will not be able to find them.
* **Do not** change the names of the objects. They will automatically be named the same thing as the prefab. This is critical for the levelLoader. The names should look like *Sphere, Sphere (1) ...* .
* Make sure you only press the Generate button **ONCE**. Otherwise you'll get duplicates.

Here is an example of the structure:
Take note of the tree-structure under *pappa*.
![There was supposed to be a picture here](https://snipboard.io/GFxle0.jpg "Example")

### 5. Add the Scene to the controller

Now, all of the needed data will be saved in your Scene scriptableobject. All you need to do is add that Scene to the controller.
