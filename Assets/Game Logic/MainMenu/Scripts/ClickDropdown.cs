﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(TMP_Dropdown))]

public class ClickDropdown : MonoBehaviour

{

    public GameObject AudioPlayer;
    private AudioScript source;
    public TMP_Dropdown youreDropdown;


    // Start is called before the first frame update
    void Start()
    {
        TMP_Dropdown dropdown = youreDropdown.GetComponent<TMP_Dropdown>();
        source = AudioPlayer.GetComponent<AudioScript>();
        dropdown.onValueChanged.AddListener((value) => OnClick());
    }

    public void OnClick()
    {
        source.PlaySound();
    }
}
