﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class AudioManager : MonoBehaviour
{
    public static AudioManager instance;

    public AudioMixer audioMixer;
    public AudioSetting[] audioSettings;
    private enum AudioGroups {Music, Game};

    void Awake()
    {
        instance = GetComponent<AudioManager>();
    }

    void Start()
    {
        for (int i = 0; i < audioSettings.Length; i++)
        {
            audioSettings[i].Init();
        }
    }

    public void SetMusicVolume (float value)
    {
        audioSettings[(int)AudioGroups.Music].SetParam(value);
    }

    public void SetGameVolume(float value)
    {
        audioSettings[(int)AudioGroups.Game].SetParam(value);
    }
}

[System.Serializable]
public class AudioSetting
{
    public Slider slider;
    public GameObject Xvalue;
    public string exposedParam;

    public void Init()
    {
        slider.value = PlayerPrefs.GetFloat(exposedParam);
    }

    public void SetParam (float value)
    {
        Xvalue.SetActive(value <= slider.minValue);
        AudioManager.instance.audioMixer.SetFloat(exposedParam, value);
        PlayerPrefs.SetFloat(exposedParam, value);
    }

}