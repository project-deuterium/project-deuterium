﻿using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]

public class ClickButton : MonoBehaviour

{

    public GameObject AudioPlayer;
    private AudioScript source;
    public Button youreButton;


    // Start is called before the first frame update
    void Start()
    {
        Button button = youreButton.GetComponent<Button>();
        source = AudioPlayer.GetComponent<AudioScript>();
        button.onClick.AddListener(() => OnClick());


    }

    public void OnClick()
    {
        source.PlaySound();
    }

}
