﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioScript : MonoBehaviour
{
    public AudioClip sound;

    private AudioSource source { get { return GetComponent<AudioSource>(); } }

    private void Start()
    {
        source.playOnAwake = false;
    }

    public void PlaySound()
    {
        source.clip = sound;
        source.playOnAwake = false;
        source.Play();

    }
}
