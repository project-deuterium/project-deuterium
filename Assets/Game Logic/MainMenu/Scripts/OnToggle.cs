﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Toggle))]

public class OnToggle : MonoBehaviour

{
    public GameObject AudioPlayer;
    private AudioScript source;
    public Toggle mytoggle;

    // Start is called before the first frame update
    void Start()
    {
  
        Toggle toggle = mytoggle.GetComponent<Toggle>();
        source = AudioPlayer.GetComponent<AudioScript>();
        toggle.onValueChanged.AddListener((value) => OnClick());
    }

    void OnClick()
    {
        source.PlaySound();
    }
}
