﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{

    public Controller controller;

    /* Gives functionality to the "play" button. It then switches to the game scene from the queue. */
    public void playButton()
    {
        DontDestroyOnLoad(controller);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    /* Gives functionality to the "Quit" button by closing the application */
    public void QuitGame()
    {
        Application.Quit();
    }
}
