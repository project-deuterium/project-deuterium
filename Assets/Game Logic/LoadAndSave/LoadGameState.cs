﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
using System.Globalization;
using TMPro;
using UnityEngine.SceneManagement;
using SimpleFileBrowser;

public class LoadGameState : MonoBehaviour
{
    public Controller controller;
    StreamReader streamReader;
    string path;
    public TMP_InputField PlayerInput;

    public List<(string, Vector3)> tempPrefabCoordinates;

    public void Start()
    {
        tempPrefabCoordinates = new List<(string, Vector3)>();

        TMP_InputField input = PlayerInput.GetComponent<TMP_InputField>();
    }


    public void LoadDataFromFile(string path)
    {
        streamReader = new StreamReader(path);
        string line;
        int cutoffIndex = 2;
        int start;
        int end;

        line = streamReader.ReadLine();

        while (line != null)
        {
            if (line[0].Equals('1'))
            {
                end = line.IndexOf(",");
                int length = (end - 4);
                string insertString = line.Substring(cutoffIndex + 2, length).ToString();

                start = line.IndexOf(",") + 3;
                end = line.IndexOf("]");
                length = (end - start) - 2;

                string s = line.Substring(start, length).ToString();
                string[] temp = s.Split(',');


                float x = float.Parse(temp[0], CultureInfo.InvariantCulture);
                float y = float.Parse(temp[1], CultureInfo.InvariantCulture);
                float z = float.Parse(temp[2], CultureInfo.InvariantCulture);
                Vector3 insertVector = new Vector3(x, y, z);


                tempPrefabCoordinates.Add((insertString, insertVector));

            }

            else if (line[0].Equals('2'))
            {

                int insertValue = int.Parse(line.Substring(cutoffIndex).ToString());
                controller.History.Add(insertValue);
            }


            else if (line[0].Equals('3'))
            {
                int insertValue = int.Parse(line.Substring(cutoffIndex).ToString());
                controller.CurrentNode = insertValue;
                
            }

            line = streamReader.ReadLine();
        }


        controller.PlacedDragDropPrefabs.Add((tempPrefabCoordinates));
    }


    public void onSucces(string filepath)
    {
        PlayerInput.text = filepath;
        path = filepath;
    }

    public void onCancel()
    {

    }


    public void pathSelect()
    {
        FileBrowser.SetFilters(false, new FileBrowser.Filter("Text Files", ".txt"));
        FileBrowser.ShowLoadDialog(onSucces, onCancel, false, null, "Load", "Select");
    }

    public void Load()
    {
        LoadDataFromFile(path);
        DontDestroyOnLoad(controller);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

}
