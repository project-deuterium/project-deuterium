﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
using TMPro;
using UnityEditor;
using SimpleFileBrowser;

public class SaveGameState : MonoBehaviour
{
    public Controller controller;
    TextWriter file;
    public TMP_InputField PlayerInput;
    public string path;


    private void Start()
    {
        controller = FindObjectOfType<Controller>();
        TMP_InputField input = PlayerInput.GetComponent<TMP_InputField>();
    }


    public void WriteToFile(string path)
    {
        file = new StreamWriter(path);

        foreach (var list in controller.PlacedDragDropPrefabs)
         {
            foreach(var pair in list)
            {
                file.WriteLine("1 [" + pair + "]");
            }
            
             
         } 
        
        
        foreach(int value in controller.History)
         {
             file.WriteLine("2 " + value);
         } 
        
        file.WriteLine("3 " + controller.CurrentNode); 

        file.Close();
    }

    public void onSucces(string filepath)
    {
        PlayerInput.text = filepath;
        path = filepath;
    }

    public void onCancel()
    {

    }


    public void pathSelect()
    {
        FileBrowser.SetFilters(false, new FileBrowser.Filter("Text Files", ".txt"));
        FileBrowser.ShowSaveDialog(onSucces, onCancel, false, null, "Save", "Save");
    }


    public void Save()
    {
        WriteToFile(path);
    }
}

