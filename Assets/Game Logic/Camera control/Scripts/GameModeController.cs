﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameModeController : MonoBehaviour
{
    public bool isInEditMode = true;
    private bool isGameOver = false;
    public Camera playCam, editCam;
    private Renderer playChar;
    private GameObject HUDCanvas;
    public Cursor cursor;

    // Start is called before the first frame update
    void Start()
    {
        HUDCanvas = GameObject.Find("HUDCanvas");
        setGameComponents();
        //Game starts in edit mode
        //Top down camera is used
        enableEditCam();
        //Make the character invisible for edit mode
        //playChar.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        switchMode();
    }

    public void setGameOver()
    {
        isGameOver = true;
    }

    /*
     * Press 'c' to switch between game modes. 
     */
    private void switchMode()
    {
        if (Input.GetKeyDown(KeyCode.C) && !isGameOver)
        {
            switchCurrentCamera();
            //switchTargetVisiblity();
            isInEditMode = !isInEditMode;
            //Toggle drag-n-drop HUD
            HUDCanvas.SetActive(isInEditMode);
        }
    }

    private void setGameComponents()
    {
        /*playCam = GameObject.FindGameObjectWithTag("ThirdPersonCam").GetComponent<Camera>();
        if (!GameObject.FindGameObjectWithTag("TopDownCam"))
        {
            throw new System.Exception("couldn't find topdowncam");
        }
        editCam = GameObject.FindGameObjectWithTag("TopDownCam").GetComponent<Camera>();*/
        playChar = GameObject.FindGameObjectWithTag("PlayerCharacter").GetComponent<Renderer>();
    }

    //Switches to the opposite camera mode than what is currently running
    private void switchCurrentCamera()
    {
        if (IsInEditMode())
        {
            enablePlayCam();
            Cursor.lockState = CursorLockMode.Locked;
        }
        else
        {
            enableEditCam();
            //TODO: Should probably use confined here later on as to not be able to click outside the game screen.
            Cursor.lockState = CursorLockMode.None;

        }   
    }

    private void enablePlayCam()
    {
        playCam.enabled = true;
        editCam.enabled = false;
        playCam.GetComponent<AudioListener>().enabled = true;
        editCam.GetComponent<AudioListener>().enabled = false;
        playCam.tag = "MainCamera";
    }

    public void enableEditCam()
    {
        editCam.enabled = true;
        playCam.enabled = false;
        editCam.GetComponent<AudioListener>().enabled = true;
        playCam.GetComponent<AudioListener>().enabled = false;
        editCam.tag = "MainCamera";
    }
    private bool IsInEditMode()
    {
        return isInEditMode;
    }

    //Switches to the opposite character visibility mode than what is currently running
    private void switchTargetVisiblity()
    {
        if (IsInEditMode())
        {
            //When switch happens during edit mode the play mode occurs, thus player character needs to be visible and v.v.
            playChar.enabled = true;
        }
        else
        {
            playChar.enabled = false;
        }
    }
}
