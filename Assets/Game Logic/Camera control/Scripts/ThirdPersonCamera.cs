﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
 * Guide for building camera movement https://www.youtube.com/watch?v=7nxpDwnU0uU
 */
public class ThirdPersonCamera : MonoBehaviour
{
    public Transform target;
    public float rotationSpeed = 1.0f;
    public float distance;
    private float mouseX, mouseY;
    private const float Y_ANGLE_MIN = 4.0f;
    private const float Y_ANGLE_MAX = 50.0f;
    


    // Start is called before the first frame update
    void Start()
    {
        
    }

    //Since the player position is calculated at each update the cameras position needs to be calculated after that, hence why lateupdate instead of update.
    private void LateUpdate()
    {
        CamControl();
    }

    void CamControl()
    {
        mouseX += Input.GetAxis("Mouse X") * rotationSpeed;
        mouseY -= Input.GetAxis("Mouse Y") * rotationSpeed;
        //Can't go below/above ceartain points as to avoid being able to loop around the scene.
        mouseY = Mathf.Clamp(mouseY, Y_ANGLE_MIN, Y_ANGLE_MAX);
        //Focus camera at players pov
        transform.LookAt(target);
        //Camera will rotate with target since it is a child object of target
        target.rotation = Quaternion.Euler(mouseY, mouseX, 0);
    }




}
