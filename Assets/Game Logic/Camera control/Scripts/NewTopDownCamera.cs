﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewTopDownCamera : MonoBehaviour
{
   
    private const float MAX_ZOOM = 10.0f;
    private const float MIN_ZOOM = 5.0f;

    public Transform target;
    public float height = 10.0f;
    public float distance = 30.0f;
    public float angle = 45.0f;
    public float smoothSpeed = 0.5f;
    private Vector3 refVelocity;


    // Start is called before the first frame update
    void Start()
    {
        handleCamera();
    }

    // Update is called once per frame
    void Update()
    {
        handleCamera();
    }

    private void handleCamera()
    {
        //Vectors for where camera is placed.
        Vector3 worldPosition = (Vector3.forward * -distance) + (Vector3.up * height);
        Vector3 rotateVector = Quaternion.AngleAxis(angle, Vector3.up) * worldPosition;

        Vector3 flatTargetPosition = target.position;
        flatTargetPosition.y = 0f;
        Vector3 finalPosition = flatTargetPosition + rotateVector;
        transform.position = finalPosition;
        //Moves the camera more smoothly towards its target.
        transform.position = Vector3.SmoothDamp(transform.position, finalPosition, ref refVelocity, smoothSpeed);
        transform.LookAt(flatTargetPosition);

        //For testing
        Debug.DrawLine(target.position, worldPosition, Color.red);
        Debug.DrawLine(target.position, rotateVector, Color.green);
        Debug.DrawLine(target.position, finalPosition, Color.blue);

        //Control positioning of camera 
        zoom();
        rotate();
    }

    private void zoom()
    {
        if (Input.GetAxis("Mouse ScrollWheel") > 0 && height > MIN_ZOOM)
        {
            height--;
            distance=distance-2;
        }
        if (Input.GetAxis("Mouse ScrollWheel") < 0 && height < MAX_ZOOM)
        {
            height++;
            distance =distance+2;
        }
    }

    private void rotate()
    {
        if (Input.GetKey(KeyCode.Q))
        {
            angle++;
        }
        if (Input.GetKey(KeyCode.E))
        {
            angle--;
        }
    }
    
}
