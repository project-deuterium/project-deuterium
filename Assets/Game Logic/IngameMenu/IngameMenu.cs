﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class IngameMenu : MonoBehaviour
{
    public static bool pausedGame = false;
    public GameObject ingameUI;
    private Animator canvasAnim;
    public GameModeController gamemode;

    private void Start()
    {
        canvasAnim = GetComponent<Animator>();
        ingameUI.SetActive(false);
        gamemode = FindObjectOfType<GameModeController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if(pausedGame)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }
    }

    public void Resume()
    {
        ingameUI.SetActive(false);
        Time.timeScale = 1f;
        pausedGame = false;
        canvasAnim.SetBool("Disable", true);
        if(gamemode.isInEditMode == false)
        {
            Cursor.lockState = CursorLockMode.Locked;
        }
        //Cursor.lockState = CursorLockMode.Locked;
    }


    void Pause() 
    {
        ingameUI.SetActive(true);
        Time.timeScale = 0f;
        pausedGame = true;
        canvasAnim.SetBool("Disable", false);
        if(gamemode.isInEditMode == false)
        {
            Cursor.lockState = CursorLockMode.None;
        }
        //Cursor.lockState = CursorLockMode.None;
    }

    public void MainMenu()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene("Menu");

    }

    public void QuitGame()
    {
        Application.Quit();
    }


}
