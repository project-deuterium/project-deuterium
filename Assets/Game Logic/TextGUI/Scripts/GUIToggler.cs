﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GUIToggler : MonoBehaviour
{
    public GameObject textGUI;

    public void toggleGUI()
    {
        if(textGUI != null)
        {
            bool isActive = textGUI.activeSelf;
            textGUI.SetActive(!isActive);
        }
    }
}
