﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.IO;
using System;

public class StoryTextHandler : MonoBehaviour
{
    public TextMeshProUGUI storyText;
    public Button nextButton;
    public Button backButton;
    public int lineLimit;

    private string sceneText;
    private string firstChoice;
    private string secondChoice;
    private String[] parsedScene;
    private String[] cDelim = { "*" };
    private String[] tDelim = { "_" };
    private int counter;
    private List<(int, int)> indexList;


    // Start is called before the first frame update
    void Start()
    {
        //This is called from templateLoader
        //displayText("scene3"); 

        nextButton.onClick.AddListener(Forward);
        backButton.onClick.AddListener(Back);
    }

    // Update is called once per frame
    void Update()
    {
        toggleButtons();     
    }

    /**
    * This method moves backwards in the text when a back button is pressed,
    * it moves by a set amount (lineLimit) the start of the text is reached.
    */
    public void Back()
    {
        counter--;
        Debug.Log("start " + indexList[counter].Item1 + "end " + indexList[counter].Item2);
        storyText.text = listToString(indexList[counter].Item1, indexList[counter].Item2);
    }

    /**
     * This method moves forward in the text when a continue button is pressed,
     * it moves by a set amount (lineLimit) the end ot the text is reached.
     */
    public void Forward()
    {
        counter++;
        //Debug.Log("start " + indexList[counter].Item1 + "end " + indexList[counter].Item2);
        storyText.text = listToString(indexList[counter].Item1, indexList[counter].Item2);
        if(counter == indexList.Count - 1)
        {
            storyText.text += displayChoices();
        }

    }

    /**
     * This method formats and returns the choices of the scene file as a string.
     */
    private string displayChoices()
    {
        return "\n" + firstChoice + "\n" + secondChoice;
    }

    /**
     * This method creates a string from a specified subarray
     * @start: index from where the extraction of the subarray begin
     * @end: where the range of the subarray should stop
     */
    private string listToString(int start, int end)
    {
        int len = (end - start) + 1;
        string[] selectedSegment = new string[len];
        Array.Copy(parsedScene, start, selectedSegment, 0, len);
        return string.Join("", selectedSegment);
    }

    /**
     * This method checks if the text contains to many lines compared to the lineLimit 
     * that has been set.
     */
    private bool isTextTooLong()
    {
        if(parsedScene.Length > lineLimit)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * This method toggles the visiblity of the buttons based on if they
     * are needed, i.e. if there is more text the next button is visible, 
     * if the next button has been pressed the back button is visible. 
     */
    private void toggleButtons()
    {
        if (counter > 0)
        {
            backButton.gameObject.SetActive(true);
        }
        else
        {
            backButton.gameObject.SetActive(false);
        }
        if (counter < indexList.Count-1)
        {
            nextButton.gameObject.SetActive(true);
        }
        else
        {
            nextButton.gameObject.SetActive(false);

        }
    }

    /**
     * This function calculates how many lines of the text will be shown for each 
     * button press.
     */
    private void calculatePageIndexes()
    {
        int start = 0;
        int end = lineLimit-1;
        if (parsedScene.Length % lineLimit == 0)
        {
            while (end < parsedScene.Length)
            {
                indexList.Add((start, end));
                start = end + 1;
                end += lineLimit;
            }
        }
        else
        {
            while (end < parsedScene.Length)
            {
                indexList.Add((start, end));
                if (end < (parsedScene.Length - lineLimit))
                {
                    start = end + 1;
                    end += lineLimit;
                }
                else
                {
                    start = end + 1;
                    end = parsedScene.Length;
                    indexList.Add((start, end - 1));
                }
            }
        }
    }

    /**
     * This method displays the text of the given text file in the GUI.
     * @filename: name of the file that should be display, ie "scene1.txt"
     */
    public void displayText(string filename)
    {
        //sceneText = FileToString(filename);
        sceneText = readFileForBuild(filename);
        String[] parsedChoices = parseText(sceneText, cDelim);
        resetChoices();
        setChoices(parsedChoices, parsedChoices.Length);
        parsedScene = parseText(sceneText, tDelim);
        if (isTextTooLong())
        {
            resetIndexes();
            calculatePageIndexes();
            storyText.text = listToString(indexList[counter].Item1, indexList[counter].Item2);
        }
        else
        {
            storyText.text = sceneText;
            storyText.text += displayChoices();
        }  
    }

    private void resetIndexes()
    {
        indexList = new List<(int, int)>();
        counter = 0;
    }
    private void resetChoices()
    {
        firstChoice = "";
        secondChoice = "";
    }

    //Just for debugging
    private string tupleListToString()
    {
        string str = "[ ";
       for(int k = 0; k < indexList.Count; k++)
        {
            str += "(" + indexList[k].Item1 + ", " + indexList[k].Item2 + "),";
        }
        return str + " ]";
    }

    /**
     * This method checks if the scene is an end scene or a options scene and sets
     * the results accordingly.
     * @parsedChoices: first version of the parsed text file, where delim used was *.
     * @size: lenght of parsedChoices
     */
    private void setChoices(String[] parsedChoices, int size)
    {
        firstChoice = "";
        secondChoice = "";
        if (size == 3) 
        {
            secondChoice = parsedChoices[2];
            firstChoice = parsedChoices[1];
            sceneText = parsedChoices[0];
        }
        //End scene occured
        else if(size == 2)
        {
            firstChoice = parsedChoices[1];
            sceneText = parsedChoices[0];
        }
    }

    /**
     * Parses a string based on specified delimiter.
     * @sceneText: string to parse
     * @delimiter: what char to parse on
     * @return parsedText: resluting list of the parsed text
     */
    private String[] parseText(string sceneText, String [] delimiter)
    {
        String[] parsedText = sceneText.Split(delimiter, 100, StringSplitOptions.RemoveEmptyEntries);
        return parsedText;

    }

    /**
     * This method reads contents of a file and saves it to a string.
     * @filename: file to read from, only name needed, no .txt.
     * @returns: string made from file content.
    */
    private string readFileForBuild(string filename)
    {
        TextAsset file = Resources.Load<TextAsset>(filename);
        Debug.Log(file.ToString());
        return file.ToString();
    }

    //Deprecated
    private string FileToString(string filename)
    {
        string text = "";
        //TODO: update to the actual path of the files
        string path = "Assets/SceneTexts/" + filename;
        try
        {
            using (StreamReader reader = new StreamReader(path))
            {
                text = reader.ReadToEnd();
            }
        }
        catch (Exception e)
        {
            Debug.Log("File could not be read");
            Debug.Log(e.Message);
        }
        return text;  
    }


}
