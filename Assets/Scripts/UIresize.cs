﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIresize : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        transform.localScale = new Vector3(Screen.currentResolution.width*0.15f, Screen.currentResolution.height *1.1f, 1);
        
    }

    // Update is called once per frame
    void Update()
    {
    }
}
