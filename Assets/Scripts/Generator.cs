﻿using System.Collections.Generic;
using UnityEngine;

public class Generator : MonoBehaviour
{
    [SerializeField] Scene scene;

    public void generateScene()
    {
        GameObject parent = GameObject.Find("pappa"); // Generated with all obj.
        List<GameObject> children = new List<GameObject>();
        foreach (Transform child in parent.transform)
        {
            children.Add(child.gameObject);
        }
        generateScene(children);
    }

    public void generateScene(List<GameObject> gameObjects)
    {
        // Picks last scene in scenelist
        foreach (GameObject obj in gameObjects)
        {
            string name = parseName(obj);

            scene.ScenePrefabs.Add(name);
            scene.PrefabCoordinates.Add(obj.transform.position);
            scene.PrefabRotation.Add(obj.transform.localRotation.eulerAngles);
            scene.PrefabScaling.Add(obj.transform.localScale);

        }
        ForceSerialization(); // Force save

    }

    /// <summary>
    /// Helper method to get the prefab name from the object name. Will remove (1) from names etc.
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    private string parseName(GameObject obj)
    {
        string[] nameParts = obj.name.Split(' ');
        return nameParts[0];

    }

    void ForceSerialization()
    {
        #if UNITY_EDITOR
        UnityEditor.EditorUtility.SetDirty(scene);
        #endif
    }


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
