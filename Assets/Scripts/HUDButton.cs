﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HUDButton : MonoBehaviour
{
    HUDController hudcontroller;
    [SerializeField] GameObject prefabToSet;
    // Start is called before the first frame update
    void Start()
    {
        hudcontroller = FindObjectOfType<HUDController>();

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnMouseDown()
    {
        hudcontroller.GetComponent<BoxCollider>().enabled = true;
        hudcontroller.SetPrefab(prefabToSet);


    }

}
