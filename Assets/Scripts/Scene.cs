﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Scene", menuName = "ScriptableObjects/Scene", order = 1)]

public class Scene : ScriptableObject
{
    public string NarrativeTextEasy;
    public string NarrativeTextHard;
    public List<Vector3> Anchorpoints;
    public List<string> SignificantDragDropPrefabs; // No coordinates, the templateLoader can decide where to initially place.
    public List<string> UnsignificantDragDropPrefabs; // No coordinates, the templateLoader can decide where to initially place.
    public List<string> ScenePrefabs; // List of prefabs in the "background" of the scene.
    public List<Vector3> PrefabCoordinates;
    public List<Vector3> PrefabScaling;
    public List<Vector3> PrefabRotation;

    public AudioClip NarrativeAudio;
    public AudioClip AmbientAudio;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
