using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TemplateLoader : MonoBehaviour
{
    Controller controller;
    GameObject objectsInScene;
    [SerializeField] GameObject nextShowcaseSceneBtn;
    [SerializeField] GameObject prevShowcaseSceneBtn;
    [SerializeField] GameObject gameOverCanvas;
    [SerializeField] private GameObject wipeCanvas;
    [SerializeField] private GameObject anchorPrefab;
    [SerializeField] private GameObject HUDObjects;
    [SerializeField] private GameObject player;
    private GameObject[] anchorpoints;
    private GameObject[] significantObj;
    private bool occupied;
    private GameObject text;

    Animator canvasAnim;

    // Start is called before the first frame update
    void Start()
    {
        
        if (!GameObject.Find("ObjectsInScene"))
        {
            objectsInScene = new GameObject("ObjectsInScene");
        }
        else
        {
            objectsInScene = GameObject.Find("ObjectsInScene");
        }

        controller = FindObjectOfType<Controller>();
        canvasAnim = wipeCanvas.GetComponent<Animator>();
        Scene firstSceneToLoad = controller.StartupScene();
        LoadObjectsFromScene(firstSceneToLoad);

    }
    //Starts the coroutine to load the next scene
    public void LoadNextScene()
    {
        text = GameObject.FindGameObjectWithTag("Errormessage");// For error messaging
        
        //Next scene if objects is placed

        checkPlacement();
        if (occupied)
        {
            StartCoroutine(StartAnimAndLoad());
            text.GetComponent<TMPro.TextMeshProUGUI>().text = "";

        }
        else
        {
            //objects not placed.
            text.GetComponent<TMPro.TextMeshProUGUI>().text = "Placera ut objekt i världen";
        }
    }

    private void checkPlacement()
    {
        occupied = false;
        //Check if object is placed on anchor point
        significantObj = GameObject.FindGameObjectsWithTag("SignificantObj");
        anchorpoints = GameObject.FindGameObjectsWithTag("anchorpoint");
        foreach (GameObject obj in significantObj)
        {
            foreach (GameObject anchor in anchorpoints)
            {
                float dist = Vector3.Distance(obj.transform.position, anchor.transform.position);
                if (dist == 0)
                {
                    //Anchorpoint is still occupied
                    occupied = true;
                }
                else
                {
                    if (occupied) break;
                    else
                    {
                        occupied = false;
                    }
                }
            }
        }
    }

    //Starts the animation for the screen wipe and removes and saves the old objects in the scene and loads the next scene
    IEnumerator StartAnimAndLoad()
    {
        canvasAnim.SetTrigger("scrollIn");
        yield return new WaitForSeconds(1f);
        

        
        if (objectsInScene.transform.childCount > 0)
        {
            GameObject[] SigObjToSave = GameObject.FindGameObjectsWithTag("SignificantObj");
                GameObject[] UnsigObjToSave = GameObject.FindGameObjectsWithTag("UnSignificantObj");
                if (SigObjToSave.Length > 0)
                {
                    SaveOldObjectsInController(SigObjToSave);
                }
                if (UnsigObjToSave.Length > 0)
                {
                    SaveOldObjectsInController(UnsigObjToSave);
                }

                
        }

        Scene nextScene = controller.SwitchScene();
        if(nextScene != null)
        {
            DestroyOldObjects(objectsInScene);
            //If there are dragable objects in the HUD they are destroyed so that the new ones can be loaded
            if (HUDObjects.transform.childCount > 0)
            {
                foreach (Transform child in HUDObjects.transform)
                {
                    if (child.GetComponent<HUDButton>())
                    {
                        Destroy(child.gameObject);
                    }
                }
            }
            LoadObjectsFromScene(nextScene);
            canvasAnim.SetTrigger("scrollOut");
            
            if (GameObject.FindGameObjectWithTag("GameOver") != null)
            {
                gameOverCanvas.SetActive(true);
            }
            
        }
        else
        {
            text = GameObject.FindGameObjectWithTag("Errormessage");
            text.GetComponent<TMPro.TextMeshProUGUI>().text = "woops " + controller.CurrentNode;
        }
        
        

        
    }

    private void ReInstantiatePlayer(Vector3 offset)
    {
        if (GameObject.Find("Player"))
        {
            Destroy(GameObject.Find("Player"));
        }
        //TODO: the y coordinate should probably be an ground offset from the scene
        
        GameObject newPlayer = Instantiate(player, new Vector3(offset.x +2, offset.y, offset.z+2), player.transform.rotation);
        newPlayer.transform.parent = objectsInScene.transform;
        newPlayer.layer = 9;
        FindObjectOfType<NewTopDownCamera>().target = newPlayer.transform;
        FindObjectOfType<GameModeController>().playCam = FindObjectOfType<ThirdPersonCamera>().GetComponent<Camera>();
        FindObjectOfType<GameModeController>().enableEditCam();
    }

    //Loads objects from a scene object into the scene and HUD
    private void LoadObjectsFromScene(Scene scene)
    {
        List<string> prefabsToLoad = scene.ScenePrefabs;
        List<string> dragnDropObj = new List<string>();
        List<string> UnsignificantDragDrop = scene.UnsignificantDragDropPrefabs;
        List<string> SignificantDragDrop = scene.SignificantDragDropPrefabs;
        dragnDropObj.AddRange(SignificantDragDrop);
        dragnDropObj.AddRange(UnsignificantDragDrop);
        FindObjectOfType<DragObjController>().SetDragableObjects(dragnDropObj);

        List<Vector3> positions = scene.PrefabCoordinates;
        List<Vector3> rotations = scene.PrefabRotation;
        List<Vector3> scaling = scene.PrefabScaling;
        List<Vector3> anchorpoints = scene.Anchorpoints;

        LoadPrefabs(prefabsToLoad, positions, scaling, rotations);
        DestroyOldAnchorpoints();
        LoadAnchorpoints(anchorpoints);
        ReInstantiatePlayer(anchorpoints[0]);
        FindObjectOfType<StoryTextHandler>().displayText(scene.NarrativeTextEasy);
        LoadDragnDropObj(dragnDropObj);
    }

    private static void DestroyOldAnchorpoints()
    {
        foreach (GameObject anchor in GameObject.FindGameObjectsWithTag("anchorpoint"))
        {
            Destroy(anchor);
        }
    }

    private void LoadAnchorpoints(List<Vector3> positions)
    {
        foreach(Vector3 pos in positions)
        {
            Instantiate(anchorPrefab, pos, anchorPrefab.transform.rotation);
        }
    }

    //Instantiates objects by path at the given positions
    private void LoadPrefabs(List<string> objectsToLoad, List<Vector3> positions, List<Vector3> scaling, List<Vector3> rotations)
    {
        for (int i = 0; i < objectsToLoad.Count; i++)
        {
            GameObject obj = Instantiate(Resources.Load(objectsToLoad[i])) as GameObject;
            obj.transform.position = positions[i];
            obj.transform.localScale = scaling[i];
            obj.transform.rotation = Quaternion.Euler(rotations[i]);
            obj.transform.parent = objectsInScene.transform;
            
        }
    }

    //Overload without scaling
    private void LoadPrefabs(List<string> objectsToLoad, List<Vector3> positions)
    {
        for (int i = 0; i < objectsToLoad.Count; i++)
        {
            GameObject obj = Instantiate(Resources.Load(objectsToLoad[i])) as GameObject;
            obj.transform.position = positions[i];
            obj.transform.parent = objectsInScene.transform;
        }
    }

    //Instantiates the objects that will appear in the HUD
    private void LoadDragnDropObj(List<string> dragnDropObj)
    {
        int max = 0;
        if (dragnDropObj.Count > 5)
        {
            max = 5;
        }
        else
        {
            max = dragnDropObj.Count;
        }

        for (int i = 0; i < max; i++)
        {
            Debug.Log("poop" + i + dragnDropObj[i]);
            GameObject obj = Instantiate(Resources.Load(dragnDropObj[i]), HUDObjects.transform) as GameObject;
            string pointName = "Point" + (i + 1).ToString();
            obj.transform.position = GameObject.Find(pointName).transform.position;
//            obj.AddComponent<drag>();    //Should not be here but will avoild error.
                                         //This will be set in the scene generator
            obj.GetComponent<drag>().enabled = false;
        }
        
    }

    //The argument should be an object that is a parent to other objects. This function will destroy its children
    private static void DestroyOldObjects(GameObject objects)
    {
        foreach (Transform child in objects.transform)
        {
            Destroy(child.gameObject);
        }
    }

    //The argument should be an object that is a parent to other objects. This function will save its children
    private void SaveOldObjectsInController(GameObject[] objects)
    {
        foreach (GameObject child in objects)
        {
            controller.savePlacedPrefab(child.name, child.transform.position);
        }
    }
    
    //Gets the next showcase scene and loads it
    public void LoadNextShowcaseScene()
    {
        var nextShowcaseScene = controller.GetNextShowcaseScene();
        CheckNextAndPreviousShowcaseScene(nextShowcaseScene.Item3, nextShowcaseScene.Item4);

        Scene scene = nextShowcaseScene.Item1;
        List<(string, Vector3)> objectsAndPositions = nextShowcaseScene.Item2;
        StartCoroutine(LoadShowcaseScene(scene, objectsAndPositions));
    }

    //Gets the previous showcase scene and loads it
    public void LoadPreviousShowcaseScene()
    {
        var nextShowcaseScene = controller.GetPreviousShowcaseScene();
        CheckNextAndPreviousShowcaseScene(nextShowcaseScene.Item3, nextShowcaseScene.Item4);

        Scene scene = nextShowcaseScene.Item1;
        List<(string, Vector3)> objectsAndPositions = nextShowcaseScene.Item2;
        StartCoroutine(LoadShowcaseScene(scene, objectsAndPositions));
    }

    private void CheckNextAndPreviousShowcaseScene(bool hasNextShowcaseScene, bool hasPrevShowcaseScene)
    {
        if (hasNextShowcaseScene)
        {
            nextShowcaseSceneBtn.GetComponent<Button>().interactable = true;
        }
        else
        {
            nextShowcaseSceneBtn.GetComponent<Button>().interactable = false;
        }

        if (hasPrevShowcaseScene)
        {
            prevShowcaseSceneBtn.GetComponent<Button>().interactable = true;
        }
        else
        {
            prevShowcaseSceneBtn.GetComponent<Button>().interactable = false;
        }
    }

    /* The input should be a tuple that contains a scene object with the standard objects for that scenen and a list of
     * other objects that the user has put in the scene and their positions
     * Starts the screen wipe animation and destroys the old objects in the scene WITHOUT saving them in the controller
     * and then loads the objects from the scene object as well as the other objects*/
    IEnumerator LoadShowcaseScene(Scene showcaseScene, List<(string, Vector3)> objectsAndPositions)
    {
        canvasAnim.SetTrigger("scrollIn");
        yield return new WaitForSeconds(1f);
        DestroyOldObjects(objectsInScene);
        LoadObjectsFromScene(showcaseScene);

        List<string> prefabs = new List<string>();
        List<Vector3> pos = new List<Vector3>();
        foreach ((string, Vector3) tuple in objectsAndPositions)
        {
            prefabs.Add(tuple.Item1);
            pos.Add(tuple.Item2);
        }
        LoadPrefabs(prefabs, pos);
        canvasAnim.SetTrigger("scrollOut");
    }
}
