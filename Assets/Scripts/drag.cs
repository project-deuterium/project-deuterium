﻿/* 
 *This should be placed on every draggable object.   
 * */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class drag : MonoBehaviour
{
    public bool dragging = false;
    int maskLayer = (1 << 11);
    int dragLayer = (1 << 10);
    int environmentLayer = (1 << 8);
    private float distance;

    private void Start()
    {
        maskLayer = dragLayer | environmentLayer;
    }

    void OnMouseEnter()
    {
        //GetComponent<Renderer>().material.color = mouseOverColor; 
    }

    void OnMouseExit()
    {
        //GetComponent<Renderer>().material.color = originalColor; 
    }

    void OnMouseDown()
    {
        dragging = true;
        if (GetComponent<MeshCollider>())
        {
            GetComponent<MeshCollider>().enabled = false;
        }
        else
        {
            GetComponent<BoxCollider>().enabled = false;
        }

    }

    private void OnMouseUp()
    {
        dragging = false;
        if (GetComponent<snapTo>())
        {
            GetComponent<snapTo>().SnapToAnchorpoint();
        }
        if (GetComponent<MeshCollider>())
        {
            GetComponent<MeshCollider>().enabled = true;
        }
        else
        {
            GetComponent<BoxCollider>().enabled = true;
        }
    }

    void Update()
    {
        RaycastHit hit;



        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit, Mathf.Infinity, maskLayer))
        {

            if (hit.collider.tag == "Environment")
            {
                if (dragging)
                {
                    Vector3 rayPoint = hit.point;
                    rayPoint.y += 0f; //Offset for object on plane 
                    transform.position = rayPoint;
                }

            }

        }
    }
}
