﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragObjController : MonoBehaviour
{
    [SerializeField] private GameObject points;
    [SerializeField] private GameObject HUDObjects;
    private List<string> dragableObjects;
    int minPointer;
    int maxPointer;

    private void Start()
    {
        minPointer = 0;
        maxPointer = minPointer + 5;
    }

    public void SetDragableObjects(List<string> obj)
    {
        dragableObjects = obj;
    }

    public void CycleUp()
    {
        DestroyOldDragObj();

        minPointer -= 5;
        if(minPointer < 0)
        {
            minPointer = 0;
        }
        maxPointer = minPointer + 5;

        InstantiateNewDragObj();
    }

    public void CycleDown()
    {
        DestroyOldDragObj();

        minPointer += 5;
        if (minPointer >= dragableObjects.Count)
        {
            minPointer -= 5;
        }

        maxPointer = minPointer + 5;
        if (maxPointer >= dragableObjects.Count)
        {
            maxPointer = dragableObjects.Count;
        }

        InstantiateNewDragObj();
    }

    private void InstantiateNewDragObj()
    {
        int i = minPointer;
        foreach (Transform point in points.transform)
        {
            GameObject obj = Instantiate(Resources.Load(dragableObjects[i]), HUDObjects.transform) as GameObject;
            obj.transform.position = point.position;
            obj.GetComponent<drag>().enabled = false;
            i += 1;
            if (i >= maxPointer)
            {
                break;
            }
        }
    }

    private void DestroyOldDragObj()
    {
        foreach (Transform child in HUDObjects.transform)
        {
            if (child.GetComponent<HUDButton>())
            {
                Destroy(child.gameObject);
            }
        }
    }
}
