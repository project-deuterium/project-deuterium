﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HUDController : MonoBehaviour
{
    private GameObject prefabToRender;
    RaycastHit hit;
    Ray ray;
    int maskLayer = (1 << 11);
    int deleteLayer;
    int dragLayer = (1 << 10);
    int environmentLayer = (1 << 8);
    private bool prefabHasBeenSet = false;
    private bool deleteMode = false;
    // Start is called before the first frame update
    void Start()
    {
        deleteLayer = dragLayer | environmentLayer;

        if (!GameObject.Find("ObjectsInScene"))
        {
            var objinscene = new GameObject("ObjectsInScene");
        }
        GetComponent<BoxCollider>().size = new Vector3(Screen.currentResolution.width * 0.85f, Screen.currentResolution.height * 2f, 1);
        //transform.localPosition = new Vector3(Screen.currentResolution.width*0.15f, transform.localScale.y, transform.localScale.z); 
    }

    // Update is called once per frame
    void Update()
    {

        if (prefabHasBeenSet)
        {
            RenderPrefab();
        }
    }

    public void SetPrefab(GameObject prefab)
    {
        deleteMode = false;
        //Debug.Log("skapa objekt");
        prefabToRender = Instantiate(prefab, GameObject.Find("ObjectsInScene").transform);
        Destroy(prefabToRender.GetComponent<HUDButton>());
        //prefabToRender.GetComponent<BoxCollider>().enabled = false;
        prefabToRender.GetComponent<SphereCollider>().enabled = false;
        prefabToRender.GetComponent<BoxCollider>().enabled = false;
        if (prefabToRender.GetComponent<MeshCollider>())
        {
            prefabToRender.GetComponent<MeshCollider>().enabled = false;
        }
        Destroy(prefabToRender.GetComponent<drag>());
        prefabHasBeenSet = true;
        //Prefab tag should be set earlier        
    }

    private void RenderPrefab()
    {
        ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit, Mathf.Infinity, ~maskLayer))
        {
            if (hit.collider.tag == "Environment" || hit.collider.tag == "anchorpoint")
            {
                prefabToRender.transform.position = hit.point;

            }
        }

    }

    public void PlacePrefab()
    {
        /*
         * Add tag to significant objects. 
         * */

        GameObject obj = Instantiate(prefabToRender,
            prefabToRender.transform.position,
            prefabToRender.transform.rotation);
        obj.transform.parent = GameObject.Find("ObjectsInScene").transform;
        if (prefabToRender.GetComponent<MeshCollider>())
        {
            obj.GetComponent<MeshCollider>().enabled = true;
            obj.GetComponent<BoxCollider>().enabled = false;
        }
        else
        {
            obj.GetComponent<BoxCollider>().enabled = true;
        }

        //obj.AddComponent<drag>();
        obj.layer = 10; //set object to drag layer

        obj.AddComponent<drag>();
        obj.layer = 10; //set object to drag layer 

        //check if object is significant
        if (obj.tag == "SignificantObj")// && GetComponent<snapTo>()) 
        {
            obj.AddComponent<snapTo>();
            obj.GetComponent<snapTo>().SnapToAnchorpoint();

        }
        prefabHasBeenSet = false;
        Destroy(prefabToRender);
    }
    public void DeleteMode()
    {
        if (deleteMode)
        {
            deleteMode = false;
        }
        else
        {
            deleteMode = true;
            GetComponent<BoxCollider>().enabled = true;


        }
        Debug.Log("delete mode: " + deleteMode);
    }

    private void OnMouseDown()
    {

        if (deleteMode)
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, dragLayer))
            {
                if (hit.collider.tag == "SignificantObj")
                {
                    Debug.Log("destroy");
                    Destroy(hit.transform.gameObject);

                }

            }
        }
        if (prefabHasBeenSet)
        {
            GetComponent<BoxCollider>().enabled = true;
            PlacePrefab();
        }
        else
        {

        }

        //Check if object is significant and if it's snappable. 
        if (tag == "SignificantObj")
        {
            GetComponent<snapTo>().SnapToAnchorpoint();
        }

    }

    private void OnMouseUp()
    {
        if (!deleteMode)
        {
            GetComponent<BoxCollider>().enabled = false;
        }

    }
}
