﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NodeCondition", menuName = "ScriptableObjects/NodeCondition", order = 3)]
public class NodeCondition : ScriptableObject
{
    public string prefab; // Placed prefab, makes condition with coordinates.
    public Vector3 coordinates; // Coordinates of prefab, makes condition.
    public NarrativeNode NextNode; // Node to be switched to if condition is met.



    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
