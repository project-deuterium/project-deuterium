﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NarrativeNode", menuName = "ScriptableObjects/Node", order = 2)]
public class NarrativeNode : ScriptableObject
{
    public List<NodeCondition> NextNarrativeNodes; // [(Prefab,coordiante), (Prefab,coordiante)...], NextNarrativeNode. All coordinates will be the same
    public int SceneIndex; // Reference to SceneIndex associated with this NarrativeNode.


    /// <summary>
    /// Matches a list of (string, Vector3) pairs against <b>NextNarrativeNodes</b> and returns the first match.
    /// </summary>
    /// <param name="storyObjects">
    /// <returns>
    /// <b>NextNode</b> of the first match.
    /// </returns>
    public NarrativeNode BranchTo(List<(string, Vector3)> storyObjects) {
      foreach (var obj in storyObjects) {
        foreach (var cond in NextNarrativeNodes) {
          // Assumes all coordinates in `NextNarrativeNodes` are the same.
          // If we move away from only having one story-affecting anchorpoint
          // `break` have to be removed and the contional to be merged with the next conditional.
          if (obj.Item2 != cond.coordinates) break;
          if (obj.Item1 == cond.prefab) {
            return cond.NextNode;
          }
        }
      }
      throw new ArgumentException($"No story object placed on the significant anchorpoint");
    }
            
      

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
