﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Controller : MonoBehaviour
{

    [SerializeField] private List<Scene> Scenes;
    [SerializeField] private List<NarrativeNode> NarrativeNodes;
    public List<List<(string, Vector3)>> PlacedDragDropPrefabs; // Prefab, Coordinate mapping of placed prefabs, index matching History.
    public List<int> History; // List of previously visited sceneIndexes, index matching to PlacedDragDropPrefabs
    public int CurrentNode = 0;
    private int CurrentShowcaseIndex = 0; // Keeps track of which index of the showcasing we are on.


    /// <summary>
    /// Called from start() when first loading into game, either scene 0 or after a load()
    /// </summary>
    /// <returns></returns>
    public Scene StartupScene() 
    {
        return GetScene(this.NarrativeNodes[CurrentNode].SceneIndex);
    }


    /// <summary>
    /// Adds a prefab with its coordinates to the list of placed prefabs in the current scene.
    /// </summary>
    /// <param name="prefab"></param>
    /// <param name="coordinates"></param>
    public void savePlacedPrefab(string prefab, Vector3 coordinates)
    {
        int index = this.History.Count; // Index to place prefabs on
        string[] name = prefab.Split('(');
        this.PlacedDragDropPrefabs[index].Add((name[0], coordinates));
        
    }

    /// <summary>
    /// This function will switch scene internally in the state aswell as return the new scene to the caller (often template).
    /// Only call after first saving the placed objects of the template.
    /// </summary>
    /// <returns></returns>
    public Scene SwitchScene()
    {
        try
        {
            NarrativeNode next = this.NarrativeNodes[CurrentNode].BranchTo(this.PlacedDragDropPrefabs[this.History.Count]);
            this.History.Add(this.NarrativeNodes[CurrentNode].SceneIndex); // Add the current scene to history.

            this.CurrentNode = NarrativeNodes.IndexOf(next);

            return GetScene(NarrativeNodes[CurrentNode].SceneIndex);
        }
        catch (System.ArgumentException error)
        {
            Scene nullScene = null;
            return nullScene;
        }
         // find next node

         // return scene corresponding to new node.


    }

    /// <summary>
    /// Switches to the next showcase and returns all neccessary data for the showcase.
    /// Will throw IndexOutOfRangeException if no next showcase exists.
    /// </summary>
    /// <returns>
    /// A tuple containing the following: The Scene object, list of placed prefabs with their coordinates, HasNextShowcase, HasPreviousShowcase
    /// </returns>
    public (Scene, List<(string, Vector3)>, bool, bool) GetNextShowcaseScene()
    {
        if (!this.HasNextShowCaseScene())
        {
            throw new System.IndexOutOfRangeException(); // No next showcase.
        }

        this.CurrentShowcaseIndex++;
        Scene scene = GetScene(this.History[this.CurrentShowcaseIndex]); // Gets scene
        List<(string, Vector3)> placedPrefabs = this.PlacedDragDropPrefabs[this.CurrentShowcaseIndex]; // Gets placed prefabs
        return (scene, placedPrefabs, this.HasNextShowCaseScene(), this.HasPreviousShowCaseScene());

    }

    /// <summary>
    /// Switches to the previous showcase and returns all neccessary data for the showcase.
    /// Will throw IndexOutOfRangeException if no previous showcase exists.
    /// </summary>
    /// <returns>
    /// A tuple containing the following: The Scene object, list of placed prefabs with their coordinates, HasNextShowcase, HasPreviousShowcase
    /// </returns>
    public (Scene, List<(string, Vector3)>, bool, bool) GetPreviousShowcaseScene()
    {
        if (!this.HasPreviousShowCaseScene())
        {
            throw new System.IndexOutOfRangeException(); // No previous showcase.
        }

        this.CurrentShowcaseIndex--;
        Scene scene = GetScene(this.History[this.CurrentShowcaseIndex]); // Gets scene
        List<(string, Vector3)> placedPrefabs = this.PlacedDragDropPrefabs[this.CurrentShowcaseIndex]; // Gets placed prefabs
        return (scene, placedPrefabs, this.HasNextShowCaseScene(), this.HasPreviousShowCaseScene());

    }

    /// <summary>
    /// This will return a scene from the controller based on the scenes index in the list.
    /// </summary>
    /// <param name="sceneIndex"></param>
    /// <returns></returns>
    public Scene GetScene(int sceneIndex)
    {
        return Scenes[sceneIndex];
    }


    private bool HasNextShowCaseScene()
    {

        if (this.CurrentShowcaseIndex < this.History.Count - 1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private bool HasPreviousShowCaseScene()
    {
        if (this.CurrentShowcaseIndex > 0)
        {
            return true;
        }
        else
        {
            return false;
        }

    }


    // Start is called before the first frame update
    void Start()
    {
        History = new List<int>();
        PlacedDragDropPrefabs = new List<List<(string, Vector3)>>();
        
        for(int i = 0; i<57; i++)
        {
            PlacedDragDropPrefabs.Add(new List<(string, Vector3)>());
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
