﻿/*
 *This script should be placed on significant objects only.
 * Items will snap to anchor points
 * */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class snapTo : MonoBehaviour
{
    private GameObject[] anchorpoints;
    private GameObject[] significantObj;


    
    public void SnapToAnchorpoint()
    {
        /*
         *foreach item med  tag == anchorpoint
         * if(distance < x)
         *      check if unoccupied
         *          snap
         *  else:
         *      ignore
         */
         
        anchorpoints = GameObject.FindGameObjectsWithTag("anchorpoint");
        foreach(GameObject anchor in anchorpoints)
        {
            float dist = Vector3.Distance(anchor.transform.position, transform.position);
            if(dist <= 5)
            {
                //Check if unoccupied
                if (AvailableAnchorpoint(anchor))
                {
                    transform.position = anchor.transform.position + new Vector3(0, 0, 0);
                    anchor.GetComponent<MeshRenderer>().enabled = false;
                    //Check if correct object or not.
                    
                }                
            }
            else
            {
                anchor.GetComponent<MeshRenderer>().enabled = true;
                //remove bool
            }
        }

    }
    public bool AvailableAnchorpoint(GameObject anchor)
    {
        /*
         * Returns true if anchorpoint is available
         * */
        bool snap = true;

        significantObj = GameObject.FindGameObjectsWithTag("SignificantObj");
        foreach (GameObject obj in significantObj)
        {
            float distToObject = Vector3.Distance(obj.transform.position, anchor.transform.position);

            if (distToObject == 0)
            {
                snap = false;
                break;
            }
            else
            {
                snap = true;
            }
        }
        return snap;
    }



}
