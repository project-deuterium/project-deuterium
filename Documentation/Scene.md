# Scene

Scene is a scriptableobject that contains all data needed to display a scene in the story.

### NarrativeTextEasy/Hard

The filename to the .txt files containing the story-text in different difficulties.

### Anchorpoints

List of coordinates to anchorpoints (currently we only have 1 anchorpoint per scene).

### Un/SignificantDragDropPrefabs

Lists containing the names of prefabs to be loaded into the drag and drop HUD. Significant means they have a meaning for the stoty.

### ScenePrefabs/Coordinates/Scaling/Rotation

List containing names of prefabs that are in the pre-created world aswell as their coordinates, scales & rotation.

### Audio

Not yet implemented but there are variables ready for ambient music aswell as narrative recordings.
