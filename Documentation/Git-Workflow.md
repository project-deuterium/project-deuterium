### Klona repot från gitlab:

`git clone git@gitlab.com:edvker-7/project-deuterium.git`

### Vid påbörjande av arbete på en issue:

1. Byt till `dev-sprint1`
   1. `git switch dev-sprint1`
2. Pulla de senaste ändringarna
   1. `git pull`
3. Gör en branch för issuen och gå till den
   1. `git switch -c \#0_Min_Fina_Issue` (Byt ut siffran mot issue nummret, och kör CamelCasing med _ separator i namnet. Detta är för att ha uniforma namn)
   2. Det går även att skapa branchen på GitLab, isåfall skippa till `punkt 5`
      1. När branchen är skapad i GitLab måste den fetchas och bytas till:
         2. `git fetch --all`
         3. `git switch \#0_Min_Fina_Issue`
4. Pusha branchen till GitLab
   1. `git push --set-upstream \#0_Min_Fina_Issue`
5. Gör saker som ska göras. Gör regelbundna commits pusha till GitLab:
   1. `git add .`
   2. `git commit -m "Ett beskrivande meddelande"`
   3. `git push`

### För att göra en merge:

1. Gör en Merge Request på GitLab
   1. Välj rätt branch. Merga till `dev-sprint1`
   2. Skriv en kort beskrivning på vad som gjorts. Inkludera `#0` i beskrivning. Byt ut nollan på siffran för rätt issue.
   3. Som `Assignee`, ange dig själv samt Edvin.
   4. Sätt rätt `Milestone`
   5. Sätt eventuellt rätt `Labels`
   6. Bocka för `Delete source...` och `Squish commits...`
2. Vänta på minst `4 upvotes` på merge-requesten, varav en måste vara från Edvin
3. Merga brancherna
   1. Om det går att göra via GitLab, tryck på Merge
   2. Om inte:
      1. `git switch \#0_Min_Fina_Branch`
      2. `git pull`
      3. `git merge dev-sprint1`
      4. `git switch dev-sprint1`
      5. `git push`

