# Steg för att skapa HUDobjekt i scenen:
1. Bestäm dig för vilka prefabs du ska ha i HUDen. 
2. Duplicera dessa prefabs och döp dem till orginalnamn_HUD. Alltså t.ex table_HUD. 
3. Dra de till HUDCanvas -> HUDObjects
4. Skala upp dessa objekt så att de passar i HUDen. Testa i scene preset 1 så att objektet ser rimligt stort ut. Jämför med den kuben som redan finns. Ungefär samma storlek. Om du går in i HUDCanvas->Background Image->Points så ser du 5 rutor. Varje HUD element kommer placeras i en sådan rektangel så se till att objektet får plats. 
5. Lägg till komponenter: 
    1. HUD Button (script)
    2. Drag (Script) och disablea det. 
    3. Box collider (ändra storlek på denna så den passar objektet. Tryck på edit collider för att justera. Hela objektet ska få plats. 
    4. Sphere Collider & bocka i is trigger. (ändra storlek på denna så den också passar objektet. Det är denna collider som avgör hur nära musen måste vara för att man ska kunna dra objektet. Man kan behöva ändra center position. Jämför med cube så ser ni ungefär vilka proportioner. Rådfråga max/olof för hjälp)
    5. Sätt tag till SignificantObj eller UnSignificantObj beroende på objektet.(Detta avgör om objektet kan snapTo anchorpoints. 
6. Kom ihåg att spara prefaben i resources. Högst uppe till höger Overrides -> apply all. 
7. Snyggt! Nu är din prefab skapad. Dra denna prefab_HUD till Significant Drag Drop Prefabs eller Unsignificant Drag Drop Prefabs(beroende på vilket av de du gör).
8. Klar! Gör nu om detta för alla objekt. 

9. Den ursprungliga prefaben som ni kopierade ska ha rätt storlek. Prova detta genom att spela spelet, och se så att objeken som ni drar ut är av rätt storlek. Om inte så får ni ändra prefaben så att den får lagom storlek. Gör det genom override-> apply all i gameobjeket.
