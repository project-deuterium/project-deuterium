# Workflow

This document contains some rules and useful tips which are meant to help keep our developing as smooth as possible.

### Merge requests

All changes (except spelling errors and such) shall be merged to the development branch via merge requests.
The person that posted the merge request merges when at least 4 other developers approve. I suggest we approve by giving the merge request a "tumbs up".
Developers reviewing the request are encouraged to leave useful comments on what could be improved.
Alla ändringar (förutom stavfel och liknande) skall mergas via merge requests.

### Branches

Try to keep one branch per issue. It's a good idea to name the branch to 5_branchname where 5 stands for the issue number and branchname should be a short describing name of the developers chosing.
We will have a development branch per sprint, this acts as our "master" during the whole sprint. We do merge requests toward this development branch, not master.
We merge the development branch with master at the end of each sprint.

### Unity

The project is done with Unity version 2019.2.10f1.

### Dokumentation

It's a good idea to continuously write comments in the code where neccessary. Also writing what a script does at the top of the file, for example "This script keeps track of the game state and provides functions to manipulate the state." for the controller script.
I suggest we also try to have basic documentation covering the system in markdown in the git-repo. Examples of these things are instructions on how to clone and setup the development, overview of the system structure and what each module does.

