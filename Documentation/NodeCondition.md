# NodeCondition

A struct defining a possible direction the story can take.

## Variables

### *string* prefab

The name of a placeable object that can affect the story AKA a story-object.

### *Vector3* coordinates

The in-world coordinates of an placed `prefab` object.

### *NarrativeNode* NextNode

The next node in the story if a placed object matches `prefab` and `coordinates`
