# Controller

The controller contains all essential data needed for the system. In addition to the data there are functions to manipulate or get the data aswell as some functions tied to system functionality.

## Data

### Scenes

Scenes is a list of all Scenes included in the story. Documentation of exactly what is in these Scenes can be found [HERE](Documentation/NarrativeNode.md).

### PlacedDragDropPrefabs

This is a list of lists. The *inner* list contains all objects the player has placed via drag and drop. Each index on the *outer* list will contain an *inner* list and each index will match the index in **History**. That is, all objects placed in sceneIndex X in History, will be stored on index X in PlacedDragDropPrefabs.

### History

This list contains the indexes(in Scenes list) of all previously visited Scenes. If the player has visited Scenes 0, 3 and 5 History will look like this: [0,3,5]. Each index in History will match the index of **PlacedDragDropPrefabs**.

### NarrativeNodes

This list contains all NarrativeNodes included in the story-tree. Further documentation of NarrativeNodes can be found [HERE].

### CurrentNode

Int value of the NarrativeNode that the player is currently on. This is only used in *play*-mode.

### CurrentShowcaseIndex

Int value of the current Showcase to be shown, is an index of **History**, only used in showcase mode.


## Functions

### savePlacedPrefab

Called from the *play*-template. Will save the given prefabs name and it's coordinates. Automatically keeps track of the index-matching of PlacedDragDropPrefabs and History, as long as it's used in a correct way: 

Only call from the *play*-template, and call this before calling SwitchScene.

### SwitchScene

This function will switch scene internally in the state aswell as return the next Scene to the caller (*player*-template). Make sure to save all placed objects with savePlacedPrefab before calling this function.

### GetNextShowcaseScene/GetPreviousShowcaseScene

This function returns the next/previous showcase from History(if there is one) aswell as all objects the player placed there. In addition to this it also returns two bools. These are true if there is a showcase previous to this, and after this respectively.

