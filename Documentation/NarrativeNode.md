# NarrativeNode



## Variables

### *List<NodeCondition>* NextNarrativeNodes

Defines the possible paths the story can take from the current *NarrativeNode*.

### *int* SceneIndex

Defines which ingame scene is associated with which *NarrativeNode*

## Methods

### *NarrativeNode* BranchTo

Receives a list of placed objects and matches them against `NextNarrativeNodes`.
There can only be one match and it throws and *ArgumentException* if there are no matches.

Returns `NextNode` from a matching *NodeCondition*.
